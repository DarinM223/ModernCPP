#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "binary_tree.hpp"
#include "queue.hpp"
#include "stack.hpp"

using namespace std;

int main(int argc, const char *argv[]) {
  data_structures::Stack<int> stack;
  stack.push(2);
  stack.push(3);

  cout << "Popping elements from the queue" << endl;
  try {
    cout << stack.pop() << endl;
    cout << stack.pop() << endl;
    cout << stack.pop() << endl; // should trigger exception
  } catch (const std::range_error &e) {
    cout << "Pop failed: " << e.what() << endl;
  }

  data_structures::Queue<int> queue;
  queue.enqueue(1);
  queue.enqueue(2);

  cout << "Dequeuing elements from the queue" << endl;
  try {
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl;
    cout << queue.dequeue() << endl; // should trigger exception
  } catch (const std::range_error &e) {
    cout << "Dequeue failed: " << e.what() << endl;
  }

  unique_ptr<data_structures::TreeNode<int>> root(
      new data_structures::TreeNode<int>(5));
  unique_ptr<data_structures::TreeNode<int>> left(
      new data_structures::TreeNode<int>(4));
  unique_ptr<data_structures::TreeNode<int>> right(
      new data_structures::TreeNode<int>(6));
  root->left = std::move(left);
  root->right = std::move(right);

  data_structures::BinaryTree<int> tree(std::move(root));

  // print the in order traversal of the tree
  cout << "In order traversal: " << endl;
  auto inorderTraversal = tree.inOrder();
  for (const auto &i : inorderTraversal) {
    cout << i << endl;
  }

  // print the post order traversal of the tree
  cout << "Post order traversal: " << endl;
  auto postorderTraversal = tree.postOrder();
  for (const auto &i : postorderTraversal) {
    cout << i << endl;
  }

  // print the pre order traversal of the tree
  cout << "Pre order traversal: " << endl;
  auto preorderTraversal = tree.preOrder();
  for (const auto &i : preorderTraversal) {
    cout << i << endl;
  }

  return 0;
}
