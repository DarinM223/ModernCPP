Basic Modern C++ data structures: The right way
===============================================

The idiomatic way to write C++ in 2016 is to use modern C++ features like smart pointers
to reduce memory leak problems. Unfortunately, modern C++ is generally not taught in schools 
even though there is a large amount of concepts that have to be learned in order to write good 
code in modern C++.

Luckily, I have been recently working with Rust a lot lately, making it easier to dabble in modern C++
since I already know most of the concepts for ownership, borrowing, and lifetimes.

# Building and running

Here's how you build the files for UNIX based systems:
```
> cmake .
> make
> ./main
```

## What is all of the constant std::unique_ptr<_> and std::move noise?

std::unique_ptr<_> is a smart pointer than enforces ownership. Owned pointers don't have to worry about 
manually destroying memory because it automatically happens when it loses scope. This cuts down on destructor boilerplate 
and is safer because you don't have to call delete. Also std::unique_ptr is essentially zero cost so there is no performance reason
to not use it.

Another useful feature is move semantics, which allow you to transfer ownership from one variable to another
using std::move. Once you move data from one variable to another, the original smart pointer becomes nullptr, so
there can only be one owner at any point in time. This is great because that means when the pointer is destroyed, 
you usually won't accidentally dereference corrupted memory. The only time I had to subvert that was with with the queue implementation
where I had to keep a dirty pointer to the back of the queue, so I had to check if the dirty pointer was going to be corrupted
whenever I dequeue the last node and set it back to nullptr.

