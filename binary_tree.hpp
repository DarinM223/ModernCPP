#pragma once

#include <vector>

namespace data_structures {

template <typename T> struct TreeNode {
  T data;

  std::unique_ptr<TreeNode<T>> left;
  std::unique_ptr<TreeNode<T>> right;

  TreeNode(T data) : data(data), left(nullptr), right(nullptr) {}

  bool isLeaf() { return (left.get() == nullptr && right.get() == nullptr); }
};

template <typename T> class BinaryTree {
public:
  BinaryTree(std::unique_ptr<TreeNode<T>> root) : root(std::move(root)) {}

  // Returns a list of elements for the inorder traversal of the tree
  std::vector<T> inOrder() {
    std::vector<T> vec;
    inOrderHelper(vec, root);
    return vec;
  }

  // Returns a list of elements for the postorder traversal of the tree
  std::vector<T> postOrder() {
    std::vector<T> vec;
    postOrderHelper(vec, root);
    return vec;
  }

  // Returns a list of elements for the preorder traversal of the tree
  std::vector<T> preOrder() {
    std::vector<T> vec;
    preOrderHelper(vec, root);
    return vec;
  }

private:
  void inOrderHelper(std::vector<T> &v, std::unique_ptr<TreeNode<T>> &root) {
    if (root->isLeaf()) {
      v.push_back(root->data);
    } else {
      inOrderHelper(v, root->left);
      v.push_back(root->data);
      inOrderHelper(v, root->right);
    }
  }

  void postOrderHelper(std::vector<T> &v, std::unique_ptr<TreeNode<T>> &root) {
    if (root->isLeaf()) {
      v.push_back(root->data);
    } else {
      inOrderHelper(v, root->left);
      inOrderHelper(v, root->right);
      v.push_back(root->data);
    }
  }

  void preOrderHelper(std::vector<T> &v, std::unique_ptr<TreeNode<T>> &root) {
    if (root->isLeaf()) {
      v.push_back(root->data);
    } else {
      v.push_back(root->data);
      inOrderHelper(v, root->left);
      inOrderHelper(v, root->right);
    }
  }

  std::unique_ptr<TreeNode<T>> root;
};

} // namespace data_structures
