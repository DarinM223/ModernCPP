#pragma once

#include <memory>
#include <stdexcept>

#include "node.hpp"

namespace data_structures {

template <typename T> class Queue {
public:
  Queue() : front(nullptr), back(nullptr) {}

  // adds an element to the queue
  void enqueue(T data) {
    auto newNode = new Node<T>(data);

    if (front.get() == nullptr) {
      front.reset(newNode);
      back = front.get();
    } else {
      back->next.reset(newNode);
      back = newNode;
    }
  }

  // returns the front element of the queue and removes it from the queue
  // throws a std::range_error on empty
  T dequeue() {
    if (front.get() == nullptr) {
      throw std::range_error("Queue is empty");
    }

    T data = front->data;
    front = std::move(front->next);

    // set back to null if front consumed the last element
    if (front.get() == nullptr) {
      back = nullptr;
    }

    return data;
  }

private:
  std::unique_ptr<Node<T>> front;
  Node<T> *back;
};

} // namespace data_structures
