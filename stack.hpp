#pragma once

#include <memory>
#include <stdexcept>

#include "node.hpp"

namespace data_structures {

template <typename T> class Stack {
public:
  Stack() : head(nullptr) {}

  // Pushes a new element onto the stack
  void push(T data) {
    auto newHead = new Node<T>(data);

    if (head.get() == nullptr) {
      head.reset(newHead);
    } else {
      newHead->next = std::move(head);
      head.reset(newHead);
    }
  }

  // Pops the top element of the stack or throws
  // a std::range_error if empty
  T pop() {
    if (head.get() == nullptr) {
      throw std::range_error("Stack is empty");
    }

    T data = head->data;
    head = std::move(head->next);
    return data;
  }

private:
  std::unique_ptr<Node<T>> head;
};

} // namespace data_structures
