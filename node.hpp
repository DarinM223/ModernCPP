#pragma once

#include <memory>

namespace data_structures {

// A singly linked-list node used for stacks and queue
template <typename T> struct Node {
  T data;
  std::unique_ptr<Node<T>> next;

  Node(T data) : data(data), next(nullptr) {}
};

} // namespace data_structures
